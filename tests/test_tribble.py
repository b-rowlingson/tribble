from tribble import __version__


def test_version():
    assert __version__ == '0.1.0'

import numpy as np
    
def test_sim():
    import tribble.sim
    a, b, r = 1,1,1
    m = tribble.sim.RI(a, b, r)
    d = np.array([0,1])
    
    assert np.all(m.f(d) == np.array([1+a, 1+a*np.exp(-1/b)]))

    
