
import numpy as np

def f(distance, alpha, beta):
    return 1 + alpha * np.exp(-(distance*distance)/beta)


def p(distance, alpha, beta, rho):
    fp = f(distance, alpha, beta)
    return (rho*fp)/(1+rho*fp)


def L(cc, distance, alpha, beta, rho):
    p = p(distance, alpha, beta, rho)
    pass

