
import pymc3 as pm
import numpy as np
import tribble.model

import pandas as pd

def test_ri():
    data = pd.DataFrame(
        {'cc': [0,0,0,0,0,1,1,1,1,1],
         'distance': [1,4,3,2,5,3.4,5,6.5,1.2,6]
        })
    f = fit_ri(data)
    return f

        

def fit_ri(data):

    nd = data['cc'].shape[0]

    coords = {"observation": data.index.values}
    
    with pm.Model(coords=coords) as ri:

        ccd = pm.Data('cc',data['cc'])
        dist = pm.Data('distance',data['distance'])
        alpha = pm.Gamma('alpha',1,1)
        beta = pm.Gamma('beta',1,1)
        rho = pm.Gamma('gamma',1,1)

        f = 1 + alpha * pm.math.exp(-(dist*dist)/beta)
        p = (1+rho*f)/(np.ones(nd)+rho*f)
        pp = (1-p)*(1-ccd) + p*ccd
        pb = pm.Binomial('prob', n=1, p=pp, dims="observation")

        fitted = pm.sample(draws=1000,tune=1000)

    return fitted

def binomial(data):
    n = 1
    coords = {"observation": data.index.values}
    with pm.Model(coords=coords) as binomial_regression_model:
        x = pm.Data("x", data["x"], dims="observation")
        # priors
        β0 = pm.Normal("β0", mu=0, sigma=1)
        β1 = pm.Normal("β1", mu=0, sigma=1)
        # linear model
        μ = β0 + β1 * x
        p = pm.Deterministic("p", pm.math.invlogit(μ), dims="observation")
        # likelihood
        pm.Binomial("y", n=n, p=p, observed=data["y"], dims="observation")

        trace = pm.sample(1000, tune=2000, return_inferencedata=True)
        return trace
        
def linear(x, y):
    with pm.Model() as simple_regression_model:
       x1 = pm.Data('x1',x)
       y1 = pm.Data('y1',y)

       # prior
       alpha = pm.Normal('intercept',0,10)
       beta = pm.Normal('coeff',0,10)
       sigma = pm.Exponential('error',lam=1)

       mu = 1+alpha + beta*x1

       y_hat = pm.Normal('y_hat',mu,sigma,observed=y1)

       trace_simple_regression = pm.sample(draws=1000,tune=1000)

    return trace_simple_regression

    
def main():
    test_ri()

if __name__=="__main__":
    main()
    
