import numpy as np
import pymc3 as pm

import tribble.model

class RI():
    def __init__(self, alpha, beta, rho):
        self.alpha = alpha
        self.beta = beta
        self.rho = rho

    def f(self, distance):
        return tribble.model.f(distance, self.alpha, self.beta)

    def p(self, distance):
        return tribble.model.p(distance, self.alpha, self.beta, self.rho)

    def generate(self, distance):
        p = self.p(distance)
        y = pm.Binomial.dist(n=1, p = p)
        return y.random(size=1)

